// console.log("Hello World!");

let username;
let password;
let role;

function getUserInput() {
	username = prompt("Input username:");
	password = prompt("Input password:");
	role = prompt("Input role:");

	if(username === "" || username === null){
		alert("Input should not be empty");
		getUserInput();
	} else if(password === "" || password === null){
		alert("Input should not be empty");
		getUserInput();
	} else if(role === "" || role === null){
		alert("Input should not be empty");
		getUserInput();
	} else {
		switch(role){
			case 'admin':
			alert("Welcome back to the class portal, admin!");
			break;
			case 'teacher':
			alert("Thank you for logging in, teacher!");
			break;
			case 'student':
			alert("Welcome to the class portal, student!");
			break;
			default:
			alert("Role out of range.");
			break;
		}
	}
}

getUserInput();


function getAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4)/4;
	average = Math.round(average);
	// console.log(average);

	if(average<=74){
		console.log( "Hello, student, your average is "+ average +". The letter equivalent is F");
	} else if(average>=75 && average<=79){
		console.log("Hello, student, your average is "+ average +". The letter equivalent is D");
	} else if(average>=80 && average<=84){
		console.log("Hello, student, your average is "+ average + ". The letter equivalent is C");
	} else if(average>=85 && average<=89){
		console.log("Hello, student, your average is "+ average +". The letter equivalent is B");
	} else if(average>=90 && average<=95){
		console.log("Hello, student, your average is "+ average + ". The letter equivalent is A");
	} else if(average>96){
		console.log("Hello, student, your average is "+ average + ". The letter equivalent is A+");
	}
}
